import yaml
import configparser
import subprocess
import tempfile
from dosbox.drives import Drive, iso, union


class DosBoxMachine:
    def __init__(self, config):
        if isinstance(config, type({})):
            self.config = config
        elif isinstance(config, type('')):
            with open(config) as file:
                self.config = yaml.load(file.read())
        else:
            raise TypeError

    def launch(self):
        drives = []
        dosbox_conf = tempfile.NamedTemporaryFile(mode='w+')

        dosbox_conf_ini = configparser.ConfigParser()
        dosbox_conf_ini.read_dict(self.config['conf'])
        dosbox_conf_ini.write(dosbox_conf)

        dosbox_conf.write('[autoexec]\n')
        for disk in sorted(self.config['disks']):
            drive = Drive.create(disk, self.config['disks'][disk])
            dosbox_conf.write(drive.mount_cmd() + '\n')
            drives.append(drive)

        dosbox_conf.write('{0}:\n'.format(self.config.get('startdisk', 'z').upper()))

        autoexec = self.config.get('autoexec', {})
        for cmd in autoexec.get('commands', []):
            dosbox_conf.write(cmd + '\n')

        dosbox_conf.flush()

        subprocess.run(['dosbox', '-conf', dosbox_conf.name])
