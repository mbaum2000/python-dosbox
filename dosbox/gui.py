import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication


def show():
    app = QApplication(sys.argv)
    ui = uic.loadUi('mainwindow.ui')
    ui.show()

    sys.exit(app.exec_())
