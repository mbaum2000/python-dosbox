from ..drives import Drive
import os
import subprocess
import tempfile


class UnionDrive(Drive):
    typename = 'union'

    def __init__(self, letter, cfg):
        super().__init__(letter, cfg)
        self.union_mount()

    def __del__(self):
        super().__del__()
        self.union_umount()

    def mount_cmd(self):
        return 'MOUNT {letter} {dir}'.format(letter=self.letter, dir=self.dir)

    def union_mount(self):
        self.dir = tempfile.mkdtemp(prefix='union-', dir=os.getcwd())
        dirs = self.config.get('path', [])

        subprocess.run(['unionfs',
                        '-o', 'cow',
                        ':'.join(reversed(dirs)),
                        self.dir])

    def union_umount(self):
        subprocess.run(['fusermount',
                        '-u', self.dir])
        os.rmdir(self.dir)
