__all__ = ['Drive', 'iso', 'union']


class DriveMeta(type):
    classes = {}

    def __new__(cls, clsname, bases, attrs):
        newclass = super(DriveMeta, cls).__new__(cls, clsname, bases, attrs)
        if 'typename' in attrs:
            DriveMeta.classes[attrs['typename']] = newclass
        return newclass


class Drive(metaclass=DriveMeta):
    def __init__(self, letter, cfg):
        self.letter = letter.upper()
        self.config = cfg
        self.dir = ''

    def __del__(self):
        pass

    def mount_cmd(self):
        raise NotImplementedError

    @staticmethod
    def create(letter, cfg):
        if cfg['type'] in DriveMeta.classes:
            return DriveMeta.classes[cfg['type']](letter, cfg)
        else:
            return None
