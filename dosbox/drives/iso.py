from ..drives import Drive


class IsoDrive(Drive):
    typename = 'iso'

    def __init__(self, letter, cfg):
        super().__init__(letter, cfg)
        self.dir = self.config.get('path')

    def __del__(self):
        super().__del__()

    def mount_cmd(self):
        return 'IMGMOUNT {letter} {dir} -t iso'.format(letter=self.letter, dir=self.dir)
